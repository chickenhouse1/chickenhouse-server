use log::error;
use std::{sync::mpsc, thread, time::Duration};

#[path = "io/door_control.rs"]
mod door_control;
// #[path = "io/gpio.rs"]
// mod gpio;

use door_control::{DoorControl, PinAssignment, SwitchTimes};
#[path = "server/server.rs"]
mod server;
use server::{NetConf, Server};

#[path = "types/my_types.rs"]
mod types;
use types::types::DoorCmds;

fn trim_blanks(to_trim: &str) -> String {
    let mut trim_cpy = to_trim.trim().to_owned();
    let mut prev_char = ' ';
    trim_cpy.retain(|ch| {
        let result = ch != ' ' || prev_char != ' ';
        prev_char = ch;
        result
    });
    trim_cpy
}

fn read_conf() -> (PinAssignment, SwitchTimes, NetConf) {
    use std::fs::File;
    use std::io::{self, BufRead};

    let mut pin_assignment = PinAssignment::new();
    let mut switch_times = SwitchTimes::new();
    let mut net_conf = NetConf::new();

    let file =
        File::open("settings.conf").expect("File \"pinout.conf\" not found in project directory");
    let lines = io::BufReader::new(file).lines();
    for line in lines.flatten() {
        let trimed_line = trim_blanks(&line);
        let (key, val): (&str, &str) = trimed_line
            .split_once(" ")
            .expect("Line in config file has invalid format");
        let (key_prefix, key_name) = key
            .split_once(".")
            .expect("Property in config file has no '.' separator");
        match key_prefix {
            "door" => pin_assignment
                .set(
                    key_name,
                    val.parse::<u8>()
                        .unwrap_or_else(|_| panic!("Pin {} number cannot be parsed", val)),
                )
                .unwrap_or_else(|err| panic!("Pin could not be assigned: {}", err)),
            "time" => switch_times
                .set(key_name, val)
                .expect("Time could not be set"),
            "net" => net_conf.set_port(val).expect("Port could not be assigned"),
            _ => {
                error!("Invalid property in config file encountered")
            }
        };
    }
    (pin_assignment, switch_times, net_conf)
}

fn main() {
    env_logger::init();
    let (pin_assignment, switch_times, net_conf) = read_conf();

    let (door_cmd_send, door_cmd_recv) = mpsc::channel();
    let (door_state_send, door_state_recv) = mpsc::channel();

    let door_control_handle =
        DoorControl::new(pin_assignment, switch_times, door_cmd_recv, door_state_send);

    let server_handle = Server::new(net_conf, door_cmd_send, door_state_recv);

    if door_control_handle.is_finished() {
        error!("DoorControl crashed. Please look at logs");
    }

    door_control_handle.join();
    server_handle.join();
    // loop {
    //     if let Some(button_press) = door_control.update_button_state() {
    //         if *button_press == PosState::Open {
    //             error!("Open button pressed");
    //             door_control.open_manually();
    //         } else if *button_press == PosState::Close {
    //             door_control.close_manually();
    //         }
    //     }
    //     thread::sleep(Duration::from_millis(100));
    // }
    // println!("Before open");
    // door_control.open();
    // println!("After open, before close");
    // let success = door_control.close();
    // println!("Closing was {}", success);
}
