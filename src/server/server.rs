mod net_msg;
use net_msg::NetCmd;

use log::{error, warn};
use std::time::Duration;

// #[path = "../io/door_control.rs"]
// mod door_control;
// use door_control::{DoorCmds, PosState};
// #[path = "../types.rs"]
use crate::{
    server::net_msg::net_cmd::TimeCmd,
    types::types::{DoorCmds, PosState},
};

use std::{
    error,
    io::Read,
    net::{TcpListener, TcpStream},
    sync::mpsc::{Receiver, Sender},
    thread::{self, JoinHandle},
};

use prost::{
    bytes::{Buf, BytesMut},
    Message,
};

use self::net_msg::net_cmd::{self, door_cmd, DoorCmd};

// #[path = "io/door_control.rs"]
// mod door_control;
// use door_control::DoorCmds;

// use self::door_control::PosState;

const RECEIVE_TIMEOUT: Duration = Duration::from_millis(200);
pub struct NetConf {
    pub port: u16,
}

impl NetConf {
    pub fn new() -> NetConf {
        NetConf { port: 9876 }
    }

    pub fn set_port(&mut self, port_str: &str) -> Result<(), &'static str> {
        let parsed_port = port_str.parse::<u16>();
        match parsed_port {
            Ok(new_port) => {
                self.port = new_port;
                Ok(())
            }
            Err(error) => Err("Parse error"),
        }
    }
}

pub struct Server {
    listener: TcpListener,
    door_cmd_send: Sender<DoorCmds>,
    door_state_recv: Receiver<PosState>,
}

impl Server {
    pub fn new(
        net_conf: NetConf,
        door_cmd_send: Sender<DoorCmds>,
        door_state_recv: Receiver<PosState>,
    ) -> JoinHandle<()> {
        thread::spawn(move || {
            let server = Server {
                listener: TcpListener::bind("0.0.0.0:".to_owned() + &net_conf.port.to_string())
                    .expect("Failed binding to port"),
                door_cmd_send,
                door_state_recv,
            };
            for stream in server.listener.incoming() {
                let mut buffer = Vec::new();
                if let Ok(buf_size) = stream.unwrap().read_to_end(&mut buffer) {
                    if let Ok(msg) = NetCmd::decode(&*buffer) {
                        server.handle_net_cmd(msg);
                    } else {
                        error!("Failed to decode buffer");
                    }
                }
            }
        })
    }

    fn handle_net_cmd(&self, net_cmd: NetCmd) -> NetCmd {
        match net_cmd.cmd.unwrap().cmd_union.unwrap() {
            net_cmd::cmd::CmdUnion::DoorCmd(door_cmd) => self.handle_door_cmd(door_cmd),
            net_cmd::cmd::CmdUnion::TimeCmd(time_cmd) => self.handle_time_cmd(time_cmd),
        }
    }

    fn handle_door_cmd(&self, door_cmd: DoorCmd) -> NetCmd {
        match door_cmd.option.try_into() {
            Ok(net_cmd::door_cmd::Option::RequestState) => {
                self.door_cmd_send
                    .send(DoorCmds::RequestState)
                    .unwrap_or_else(|err| {
                        error!("Failed to send request state to channel: {}", err)
                    });
            }
            Ok(net_cmd::door_cmd::Option::ResponseState) => {}
            Ok(net_cmd::door_cmd::Option::SetState) => {
                let target_pos = match door_cmd.state.try_into() {
                    Ok(net_cmd::door_cmd::DoorState::Open) => PosState::Open,
                    Ok(net_cmd::door_cmd::DoorState::Close) => PosState::Close,
                    Ok(net_cmd::door_cmd::DoorState::Unkown) => {
                        warn!("Received door cmd with unkown");
                        PosState::Unknown
                    }
                    Ok(net_cmd::door_cmd::DoorState::Error) => {
                        warn!("Received door cmd with error");
                        PosState::Unknown
                    }
                    Ok(net_cmd::door_cmd::DoorState::AlreadyInState) => {
                        error!("Received door cmd with alreadyinstate!");
                        PosState::Unknown
                    }
                    Err(error) => {
                        error!("Fail to convert door state to enum: {}", error);
                        PosState::Unknown
                    }
                };
                /* TODO: Remove when finished */
                let pos_str = match target_pos {
                    PosState::Close => "Close",
                    PosState::Open => "Open",
                    PosState::Unknown => "Unkown",
                    PosState::TimeState(_) => "Unkown",
                };
                println!("Send cmd for {}", pos_str);

                self.door_cmd_send
                    .send(DoorCmds::NewPos(target_pos))
                    .unwrap_or_else(|err| error!("Failed to send new pos to channel: {}", err));
            }
            Ok(net_cmd::door_cmd::Option::AcknowledgeSet) => {
                error!("Acknowledgement received. This should only be sent");
            }
            Err(error) => {
                error!("Failed to convert door option to enum: {}", error);
            }
        }
        let new_state = self
            .door_state_recv
            .recv_timeout(RECEIVE_TIMEOUT)
            .unwrap_or_else(|_| {
                warn!("Expected state update after net cmd");
                PosState::Unknown
            });
        let state_net = match new_state {
            PosState::Open => net_cmd::door_cmd::DoorState::Open,
            PosState::Close => net_cmd::door_cmd::DoorState::Close,
            PosState::Unknown => net_cmd::door_cmd::DoorState::Unkown,
            PosState::TimeState(_) => net_cmd::door_cmd::DoorState::Unkown,
        };
        let door_resp = net_cmd::cmd::CmdUnion::DoorCmd(net_cmd::DoorCmd {
            option: net_cmd::door_cmd::Option::ResponseState as i32,
            state: state_net as i32,
        });
        NetCmd {
            r#type: net_cmd::CmdType::DoorCmd as i32,
            cmd: Some(net_cmd::Cmd {
                cmd_union: Some(door_resp),
            }),
        }
    }
    fn handle_time_cmd(&self, time_cmd: TimeCmd) -> NetCmd {
        let new_door_cmd = match time_cmd.option.try_into() {
            /* TODO: Implement enablement */
            Ok(net_cmd::time_cmd::Option::EnableTime) => DoorCmds::Terminate,
            /* Implement time request  */
            Ok(net_cmd::time_cmd::Option::RequestTime) => DoorCmds::RequestState,
            Ok(net_cmd::time_cmd::Option::ResponseTime) => {
                error!("This should never come from a client!");
                DoorCmds::Terminate
            }
            Ok(net_cmd::time_cmd::Option::SetTime) => DoorCmds::NewOpenTime(time_cmd.time),
            Ok(net_cmd::time_cmd::Option::AknowledgeSet) => {
                error!("Remove this");
                DoorCmds::Terminate
            }
            Err(error) => {
                error!("Failed to match time cmd option: {}", error);
                DoorCmds::Terminate
            }
        };
        self.door_cmd_send
            .send(new_door_cmd)
            .unwrap_or_else(|err| error!("Failed to move cmd to channel: {}", err));

        let mut new_time = String::from("Unkown");
        if let PosState::TimeState(time_state) = self
            .door_state_recv
            .recv_timeout(RECEIVE_TIMEOUT)
            .unwrap_or_else(|err| {
                error!("Expected time update after time cmd: {}", err);
                PosState::Unknown
            })
        {
            new_time = time_state;
        }
        let time_update = net_cmd::cmd::CmdUnion::TimeCmd(net_cmd::TimeCmd {
            option: net_cmd::door_cmd::Option::ResponseState as i32,
            time: new_time,
        });
        NetCmd {
            r#type: net_cmd::CmdType::TimeCmd as i32,
            cmd: Some(net_cmd::Cmd {
                cmd_union: Some(time_update),
            }),
        }
    }
}
