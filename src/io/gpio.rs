use rppal::gpio::Gpio;
use rppal::gpio::InputPin;
use rppal::gpio::OutputPin;
use strum_macros::{EnumCount as EnumCountMacro, EnumIter};

#[derive(Debug, EnumCountMacro, EnumIter, Clone, Copy)]
pub enum PinRole {
    Sensor,
    SwitchPolarity,
    SwitchOpen,
    SwitchClose,
    ButtonOpen,
    ButtonClose,
}

pub struct PinAssignment {
    gpio: Gpio,
    sensor: Option<InputPin>,
    switch_polarity: Option<OutputPin>,
    switch_open: Option<OutputPin>,
    switch_close: Option<OutputPin>,
    button_open: Option<InputPin>,
    button_close: Option<InputPin>,
}

impl PinAssignment {
    pub fn new() -> PinAssignment {
        PinAssignment {
            gpio: Gpio::new().expect("Failed to create Gpio instance"),
            sensor: None,
            switch_polarity: None,
            switch_open: None,
            switch_close: None,
            button_open: None,
            button_close: None,
        }
    }

    pub fn set(&mut self, pin_name: &str, pin_num: u8) -> Result<(), &'static str> {
        let pin_obj = self.gpio.get(pin_num).expect("Unable to get pin");
        match pin_name {
            "sensor" => {
                self.sensor = Some(pin_obj.into_input());
                Ok(())
            }
            "switch_polarity" => {
                let mut output_obj = pin_obj.into_output();
                output_obj.set_low();
                self.switch_polarity = Some(output_obj);
                Ok(())
            }
            "switch_open" => {
                let mut output_obj = pin_obj.into_output();
                output_obj.set_low();
                self.switch_open = Some(output_obj);
                Ok(())
            }
            "switch_close" => {
                let mut output_obj = pin_obj.into_output();
                output_obj.set_low();
                self.switch_close = Some(output_obj);
                Ok(())
            }
            "button_open" => {
                self.button_open = Some(pin_obj.into_input_pulldown());
                Ok(())
            }
            "button_close" => {
                self.button_close = Some(pin_obj.into_input_pulldown());
                Ok(())
            }
            _ => Err("Config file contains invalid properties"),
        }
    }

    pub fn get_sensor(&self) -> &InputPin {
        self.sensor
            .as_ref()
            .expect("Trying to use sensor pin without it beeing initialized")
    }
    pub fn get_switch_polarity(&mut self) -> &mut OutputPin {
        self.switch_polarity
            .as_mut()
            .expect("Trying to use switch_polarity pin wihout it beeing initialized")
    }
    pub fn get_switch_open(&mut self) -> &mut OutputPin {
        self.switch_open
            .as_mut()
            .expect("Trying to use switch_open pin wihtout it beeing initialized")
    }
    pub fn get_switch_close(&mut self) -> &mut OutputPin {
        self.switch_close
            .as_mut()
            .expect("Trying to use switch_close pin without it beeing initialized")
    }
    pub fn get_button_open(&self) -> &InputPin {
        self.button_open
            .as_ref()
            .expect("Trying to use button_open without it beeing initialized")
    }
    pub fn get_button_close(&self) -> &InputPin {
        self.button_close
            .as_ref()
            .expect("Trying to use button_close without it beeing initialized")
    }
}
