mod gpio;
mod switch_times;
mod time_control;
pub use gpio::PinAssignment;
pub use switch_times::SwitchTimes;
pub use time_control::TimeControl;

use log::{error, warn};
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::time::Duration;

use rppal::gpio::{Level, OutputPin};

use crate::types::types::{DoorCmds, PosState};

// Timeout constants
const TICK_RATE: Duration = Duration::from_millis(100);
const RELAY_SWITCH_TIMEOUT: Duration = Duration::from_millis(100);
const RELAY_ACTIVE_TIMEOUT: Duration = Duration::from_secs(10);
const RELAY_ACTIVE_TIMEOUT_TICK: Duration = Duration::from_millis(50);
// TODO: Check if this is sufficient time to open completely
const OPEN_TIMEOUT: Duration = Duration::from_secs(4);
fn switch_and_wait(target_pin: &mut OutputPin, target_level: Level) {
    if target_level == Level::High {
        target_pin.set_high();
    } else {
        target_pin.set_low();
    }
    // Give polarity relay time to switch. If relay close is faster than relay polarity, a short circuit will occur
    thread::sleep(RELAY_SWITCH_TIMEOUT);
}
pub struct DoorControl {
    door_state: PosState,
    button_state: PosState,
    pins: PinAssignment,
    time_control: TimeControl,
}

fn send_current_state(new_state: &PosState, door_state_send: &Sender<PosState>) {
    //TODO: Enable when ready
    door_state_send
        .send(new_state.to_owned())
        .unwrap_or_else(|error| {
            error!("Failed to send new state to channel: {}", error);
        });
}

impl DoorControl {
    pub fn new(
        pin_assignment: PinAssignment,
        switch_times: SwitchTimes,
        door_cmd_recv: Receiver<DoorCmds>,
        door_state_send: Sender<PosState>,
    ) -> thread::JoinHandle<()> {
        thread::spawn(move || {
            let mut door_control = DoorControl {
                door_state: PosState::Unknown,
                button_state: PosState::Unknown,
                pins: pin_assignment,
                time_control: TimeControl::new(switch_times),
            };
            let mut terminate = false;
            while !terminate {
                // Update door state
                if let Some(door_state) = door_control.update_door_state() {
                    // send_current_state(door_state, &door_state_send);
                }
                // Handle physical button presses
                if let Some(button_press) = door_control.update_button_state() {
                    if *button_press == PosState::Open {
                        door_control.open_manually();
                    } else if *button_press == PosState::Close {
                        door_control.close_manually();
                    }
                }
                // Check if timed event occurs
                if let Some(new_state) = door_control.time_control.update_state() {
                    match *new_state {
                        PosState::Open => {
                            door_control.open();
                        }
                        PosState::Close => {
                            door_control.close();
                        }
                        PosState::Unknown => {
                            panic!("update_state returned Unknown which should not be possible");
                        }
                        PosState::TimeState(_) => {
                            panic!(
                                "update_state returned TimeState which is only used for net comm"
                            );
                        }
                    }
                }
                // Process received commands
                if let Some(new_state) = door_control.process_cmds(&door_cmd_recv, &mut terminate) {
                    send_current_state(new_state, &door_state_send);
                }
                thread::sleep(TICK_RATE);
            }
        })
    }

    fn process_cmds(
        &mut self,
        door_cmd_recv: &Receiver<DoorCmds>,
        terminate: &mut bool,
    ) -> Option<&PosState> {
        if let Ok(door_cmd) = door_cmd_recv.try_recv() {
            return match door_cmd {
                DoorCmds::NewPos(target_pos) => {
                    if *self.get_door_state() != target_pos {
                        if target_pos == PosState::Open {
                            Some(self.open())
                        } else {
                            Some(self.close())
                        }
                    } else {
                        Some(self.get_door_state())
                    }
                }
                DoorCmds::NewOpenTime(time_str) => {
                    self.time_control
                        .set_open_time(&time_str)
                        .unwrap_or_else(|error| {
                            error!(
                                "Setting open time failed from string: {} - {}",
                                time_str, error
                            )
                        });
                    Some(self.get_door_state())
                }
                DoorCmds::NewCloseTime(time_str) => {
                    self.time_control
                        .set_close_time(&time_str)
                        .unwrap_or_else(|error| {
                            error!(
                                "Setting open time failed from string: {} - {}",
                                time_str, error
                            )
                        });
                    Some(self.get_door_state())
                }
                DoorCmds::RequestState => Some(self.get_door_state()),
                DoorCmds::Terminate => {
                    *terminate = true;
                    None
                }
            };
        }
        None
    }

    /**
     * Reads door state from pin: Return None if nothing changes, otherwise the new state
     */
    pub fn update_door_state(&mut self) -> Option<&PosState> {
        let old_state = self.door_state.to_owned();
        self.door_state = if self.pins.get_sensor().read() == Level::High {
            PosState::Close
        } else {
            PosState::Open
        };
        if old_state != self.door_state {
            Some(&self.door_state)
        } else {
            None
        }
    }
    /**
     * Reads button state from pin: Returns None if nothing changes, otherwise the new state
     */
    pub fn update_button_state(&mut self) -> Option<&PosState> {
        let old_state = self.button_state.to_owned();
        self.button_state = if self.pins.get_button_open().read() == Level::High {
            PosState::Open
        } else if self.pins.get_button_close().read() == Level::High {
            PosState::Close
        } else {
            PosState::Unknown
        };
        if self.button_state != old_state {
            Some(&self.button_state)
        } else {
            None
        }
    }

    pub fn get_door_state(&mut self) -> &PosState {
        self.update_door_state();
        &self.door_state
    }

    pub fn get_button_state(&mut self) -> &PosState {
        self.update_button_state();
        &self.button_state
    }

    pub fn close(&mut self) -> &PosState {
        let open_pin = self.pins.get_switch_polarity();
        switch_and_wait(open_pin, Level::Low);
        switch_and_wait(self.pins.get_switch_polarity(), Level::Low);
        self.pins.get_switch_close().set_high();

        let mut safety_timeout = RELAY_ACTIVE_TIMEOUT;
        while *self.get_door_state() == PosState::Open {
            thread::sleep(RELAY_ACTIVE_TIMEOUT_TICK);
            if let Some(new_timeout) = safety_timeout.checked_sub(RELAY_ACTIVE_TIMEOUT_TICK) {
                safety_timeout = new_timeout;
                warn!("Timeout ocurred while trying to close");
            } else {
                break;
            }
        }
        self.pins.get_switch_close().set_low();
        self.get_door_state()
    }

    pub fn open(&mut self) -> &PosState {
        switch_and_wait(&mut self.pins.get_switch_close(), Level::Low);
        switch_and_wait(&mut self.pins.get_switch_polarity(), Level::High);
        self.pins.get_switch_open().set_high();
        thread::sleep(OPEN_TIMEOUT);
        self.pins.get_switch_open().set_low();
        self.get_door_state()
    }

    pub fn close_manually(&mut self) {
        switch_and_wait(&mut self.pins.get_switch_open(), Level::Low);
        switch_and_wait(&mut self.pins.get_switch_polarity(), Level::High);
        self.pins.get_switch_close().set_high();
        let mut safety_timeout = RELAY_ACTIVE_TIMEOUT;
        while *self.get_button_state() == PosState::Close {
            thread::sleep(RELAY_ACTIVE_TIMEOUT_TICK);
            if let Some(new_timeout) = safety_timeout.checked_sub(RELAY_ACTIVE_TIMEOUT_TICK) {
                safety_timeout = new_timeout;
            } else {
                warn!("Close button pressed until safety timeout caused");
                break;
            }
        }
        self.pins.get_switch_close().set_low();
    }

    pub fn open_manually(&mut self) {
        switch_and_wait(&mut self.pins.get_switch_close(), Level::Low);
        switch_and_wait(&mut self.pins.get_switch_polarity(), Level::Low);
        self.pins.get_switch_open().set_high();
        let mut safety_timeout = RELAY_ACTIVE_TIMEOUT;
        while *self.get_button_state() == PosState::Open {
            thread::sleep(RELAY_ACTIVE_TIMEOUT_TICK);
            if let Some(new_timeout) = safety_timeout.checked_sub(RELAY_ACTIVE_TIMEOUT_TICK) {
                safety_timeout = new_timeout;
            } else {
                warn!("Open button pressed until safety timeout caused");
                break;
            }
        }
        self.pins.get_switch_open().set_low();
    }
}
