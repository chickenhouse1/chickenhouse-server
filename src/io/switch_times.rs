use chrono::{self, NaiveTime, ParseError};

pub struct SwitchTimes {
    open_time: NaiveTime,
    close_time: NaiveTime,
}

const TIME_FORMAT_STR: &str = "%H:%M:%S";

impl SwitchTimes {
    pub fn new() -> SwitchTimes {
        SwitchTimes {
            open_time: NaiveTime::from_hms_opt(6, 0, 0).unwrap(),
            close_time: NaiveTime::from_hms_opt(21, 30, 0).unwrap(),
        }
    }

    pub fn set(&mut self, switch_prop: &str, switch_time: &str) -> Result<(), ParseError> {
        match switch_prop {
            "open" => self.set_open_time(switch_time),
            "close" => self.set_close_time(switch_time),
            _ => panic!("Tried to set switch time with invalid property"),
        }
    }

    pub fn set_open_time(&mut self, open_time_str: &str) -> Result<(), ParseError> {
        let parsed_time = NaiveTime::parse_from_str(open_time_str, TIME_FORMAT_STR);
        match parsed_time {
            Ok(new_time) => {
                self.open_time = new_time;
                return Ok(());
            }
            Err(error) => Err(error),
        }
    }

    pub fn get_open_time(&self) -> NaiveTime {
        self.open_time
    }

    pub fn set_close_time(&mut self, close_time_str: &str) -> Result<(), ParseError> {
        let parsed_time = NaiveTime::parse_from_str(close_time_str, TIME_FORMAT_STR);
        match parsed_time {
            Ok(new_time) => {
                self.close_time = new_time;
                return Ok(());
            }
            Err(error) => Err(error),
        }
    }

    pub fn get_close_time(&self) -> NaiveTime {
        self.close_time
    }
}
