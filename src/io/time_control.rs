use super::switch_times::{self, SwitchTimes};

use log::error;

use chrono::{self, Local, NaiveTime, ParseError, Timelike};

use super::PosState;

pub struct TimeControl {
    switch_times: SwitchTimes,
    prev_cmd: PosState,
}
impl TimeControl {
    pub fn new(switch_times: SwitchTimes) -> TimeControl {
        TimeControl {
            switch_times: switch_times,
            prev_cmd: PosState::Unknown,
        }
    }

    pub fn set_open_time(&mut self, open_time_str: &str) -> Result<(), ParseError> {
        self.switch_times.set_open_time(open_time_str)
    }

    pub fn set_close_time(&mut self, close_time_str: &str) -> Result<(), ParseError> {
        self.switch_times.set_close_time(close_time_str)
    }

    // This function must be called at least once every minute
    // TODO: If the program restarts in the switching minute, it is executed again since prev_cmd info is lost
    pub fn update_state(&mut self) -> Option<&PosState> {
        fn in_switching_min(check_time: NaiveTime) -> bool {
            let current_time = Local::now().time();
            current_time.minute() == check_time.minute() && current_time.hour() == check_time.hour()
        }

        if self.prev_cmd != PosState::Open && in_switching_min(self.switch_times.get_open_time()) {
            self.prev_cmd = PosState::Open;
            Some(&PosState::Open)
        } else if self.prev_cmd != PosState::Close
            && in_switching_min(self.switch_times.get_close_time())
        {
            self.prev_cmd = PosState::Close;
            Some(&PosState::Close)
        } else {
            None
        }
    }
}
