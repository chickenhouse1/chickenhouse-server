pub mod types {
    #[derive(PartialEq, Clone)]
    pub enum PosState {
        Open,
        Close,
        Unknown,
        TimeState(String),
    }

    #[derive(PartialEq)]
    pub enum DoorCmds {
        NewPos(PosState),
        NewOpenTime(String),
        NewCloseTime(String),
        RequestState,
        Terminate,
    }
}
